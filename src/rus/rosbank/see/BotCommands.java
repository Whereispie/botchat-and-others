package rus.rosbank.see;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class BotCommands extends Robot {

    public void date() { //   date – бот выводит текущую дату
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
    }

    public void time() { //  time – бот выводит текущее время
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
    }

    public void change() throws IOException { //  change – указание нового пути к файлу с возможными вариантами ответа.
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Укажите название файла, файл будет создан в папке проекта src/");
        String userInput = reader.readLine();
        String fileSeparator = System.getProperty("file.separator");
        String relativePath = "src" + fileSeparator + userInput + ".txt"; // не стал делать абсолютный путь, т.к велика вероятность java.io.WinNTFileSystem исключения
        File file = new File(relativePath);
        if (file.createNewFile()) {
            System.out.println(relativePath + " Файл был успешно создан по указанному пути");
        } else System.out.println("По указанному пути, Файл " + relativePath + " уже существует");
    }


    public void help() { //  help – помощь ;)
        System.out.println("Бот поддерживает следующие команды:");
        System.out.println("silent – бот умолкает");
        System.out.println("getUp – бот просыпается, если ранее была введена команда silent. В противном случае команда игнорируется.");
        System.out.println("date – бот выводит текущую дату]");
        System.out.println("time – бот выводит текущее время");
        System.out.println("change – указание нового пути к файлу с возможными вариантами ответа.");
        System.out.println("quit – выход из программы");
    }
}


