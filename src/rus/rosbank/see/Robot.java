package rus.rosbank.see;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;


public class Robot {

    public static void main(String[] args) throws IOException {
        BufferedReader bufSource = new BufferedReader(new InputStreamReader(new FileInputStream("src/BotLib.txt"), "windows-1251"));
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Random randomGenerator = new Random();
        ArrayList<String> botVocabulary = new ArrayList();

        while (bufSource.ready()) {
            botVocabulary.add(bufSource.readLine());
        }
        bufSource.close();

        boolean letsGo = false;
        boolean quit = false;
        System.out.println("Hello! I like random facts. For HELP please type help");

        while (!quit) {
            String humanQuestion = reader.readLine();
            int randomNumber = randomGenerator.nextInt(botVocabulary.size()); // рандомное число для ответа бота на основе длинны эррейлиста
            String botAnswer = botVocabulary.get(randomNumber);
            if (!humanQuestion.equals("quit")
                    && !humanQuestion.equals("date")
                    && !humanQuestion.equals("time")
                    && !humanQuestion.equals("change")
                    && !humanQuestion.equals("help")
                    && !humanQuestion.equals("silent")
                    && !humanQuestion.equals("getUp")) {
                System.out.println("Ohh human ! Do you know fact № " + botAnswer);
            }

            if (humanQuestion.equals("quit")) {
                quit = true;
            } else if (humanQuestion.equals("date")) {
                BotCommands getCommand = new BotCommands();
                getCommand.date();
            } else if (humanQuestion.equals("time")) {
                BotCommands getCommand = new BotCommands();
                getCommand.time();
            } else if (humanQuestion.equals("change")) {
                BotCommands getCommand = new BotCommands();
                getCommand.change();
            } else if (humanQuestion.equals("help")) {
                BotCommands getCommand = new BotCommands();
                getCommand.help();
            } else if (humanQuestion.equals("silent")) {
                boolean outOf = false;
                while (!outOf) {
                    reader.readLine();
                    System.out.println("I'm sleeping, input [getUp]");
                    if (reader.readLine().equals("getUp")) {
                        outOf = true;
                    }
                }
            }
        }
    }
}